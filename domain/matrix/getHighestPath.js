'use strict';

const { chain, sum, isArray } = require('lodash');

module.exports = (matrix) => {
    if (!isArray(matrix) || matrix.length === 0 || matrix[0].length === 0) return 0;

    const lastHorizontalIndex = matrix.length - 1;
    const lastVerticalIndex = matrix[0].length - 1;
    let i = 0, j = 0, paths = [];

    const storePath = (path, last) => {
        const completePath = path.concat(last);
        paths.push(completePath);
    }

    const isMatrixEnd = (i, j) => {
        return lastHorizontalIndex === i && lastVerticalIndex === j;
    };

    function findAllPaths(matrix, i, j, path = []) {
        if (isMatrixEnd(i, j)) {
            storePath(path, matrix[i][j]);
            return;
        }

        addCurrentElement(path, matrix, i, j);

        if (i >= 0 && i <= lastHorizontalIndex && j + 1 >= 0 && j + 1 <= lastVerticalIndex) {
            moveRight(matrix, i, j, path, findAllPaths);
        }

        if (i + 1 >= 0 && i + 1 <= lastHorizontalIndex && j >= 0 && j <= lastVerticalIndex) {
            moveDown(matrix, i, j, path, findAllPaths);
        }

        removeCurrentElement(path);
    }

    findAllPaths(matrix, i, j);

    return chain(paths)
        .map(sum)
        .max()
        .value();
}

function moveRight(matrix, i, j, path, findAllPaths) {
    findAllPaths(matrix, i, j + 1, path);
}

function moveDown(matrix, i, j, path, findAllPaths) {
    findAllPaths(matrix, i + 1, j, path);
}

function removeCurrentElement(path) {
    path.pop();
}

function addCurrentElement(path, matrix, i, j) {
    path.push(matrix[i][j]);
}
