const getHighestPath = require('./getHighestPath');

it('should return 0 when matrix is empty', () => {
    // Given
    const matrix = [];

    // When
    const highestPath = getHighestPath(matrix);

    // Then
    expect(highestPath).toEqual(0);
});

it('should return 0 when matrix is not an array', () => {
    // Given
    const matrix = 'something other then an array 😱';

    // When
    const highestPath = getHighestPath(matrix);

    // Then
    expect(highestPath).toEqual(0);
});

it('should return 25 as a highest path', () => {
    // Given
    const matrix = [[6, 3, 2, 1], [5, 5, 4, 1], [1, 2, 2, 3]];

    // When
    const highestPath = getHighestPath(matrix);

    // Then
    expect(highestPath).toEqual(25);
});

it('should return 49 as a highest path', () => {
    // Given
    const matrix = [[6, 3, 2, 13], [5, 5, 4, 1], [12, 9, 2, 3], [12, 9, 2, 3]];

    // When
    const highestPath = getHighestPath(matrix);

    // Then
    expect(highestPath).toEqual(49);
});
