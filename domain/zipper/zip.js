'use strict';

module.exports = (letters, numbers) => {
    let zippedList = [];

    letters.forEach((letter, index) => {
        zippedList.push(letter.toUpperCase(), numbers[index]);
    });

    return zippedList;
};
