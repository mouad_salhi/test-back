'use strict';

const zip = require('./zip');

it('should return zipped list with letters in uppercase', () => {
    // Given
    const letters = ['a', 'b', 'c'];
    const numbers = [1, 2, 3];
    const expectedResult = ['A', 1, 'B', 2, 'C', 3];

    // When
    const zippedList = zip(letters, numbers);

    // Then
    expect(zippedList).toEqual(expectedResult);
});
