'use strict';

const getWhileSum = require('./getWhileSum');

it('should return 0 when array is empty', () => {
    // Given
    const numbers = [0];
    const expectedResult = { result: 0 };

    // When
    const sum = getWhileSum(numbers);

    // Then
    expect(sum).toEqual(expectedResult);
});

it('should return 10', () => {
    // Given
    const numbers = [1, 2, 3, 4];
    const expectedResult = { result: 10 };

    // When
    const sum = getWhileSum(numbers);

    // Then
    expect(sum).toEqual(expectedResult);
});
