'use strict';

module.exports = (numbers) => {
    const getSum = (array) => (array.length === 0) ? 0 : array[0] + getSum(array.slice(1));
    const sum = getSum(numbers);

    return { result: sum };
};
