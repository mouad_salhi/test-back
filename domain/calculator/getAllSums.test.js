'use strict';

const getAllSums = require('./getAllSums');

it('should return the right result', () => {
    // Given
    const numbers = [1, 2, 3, 4];
    const expectedResult = { for: { result: 10 }, foreach: { result: 10 }, while: { result: 10 }, recursion: { result: 10 } };

    // When
    const sums = getAllSums(numbers);

    // Then
    expect(sums).toEqual(expectedResult);
});
