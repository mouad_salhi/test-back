'use strict';

module.exports = (numbers) => {
    let sum = 0;
    
    numbers.forEach(number => {
        sum += number;
    });

    return { result: sum };
};
