'use strict';

module.exports = (numbers) => {
    let sum = 0;
    let index = 0;

    while (index < numbers.length) {
        sum += numbers[index];
        index++;
    }

    return { result: sum };
};
