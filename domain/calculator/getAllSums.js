'use strict';

const getForSum = require('./getForSum');
const getForEachSum = require('./getForEachSum');
const getWhileSum = require('./getWhileSum');
const getRecursionSum = require('./getRecursionSum');


module.exports = (numbers) => {
    return {
        for: getForSum(numbers),
        foreach: getForEachSum(numbers),
        while: getWhileSum(numbers),
        recursion: getRecursionSum(numbers)
    };
};
