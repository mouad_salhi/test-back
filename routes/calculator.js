'use strict';

const express = require('express');
const router = express.Router();

const getForSum = require('../domain/calculator/getForSum');
const getForEachSum = require('../domain/calculator/getForEachSum');
const getWhileSum = require('../domain/calculator/getWhileSum');
const getRecursionSum = require('../domain/calculator/getRecursionSum');
const getAllSums = require('../domain/calculator/getAllSums');

router.post('/', (req, res) => {
    const sum = getAllSums(req.body.list);
    res.json(sum);
});

router.post('/for', (req, res) => {
    const sum = getForSum(req.body.list);
    res.json(sum);
});

router.post('/foreach', (req, res) => {
    const sum = getForEachSum(req.body.list);
    res.json(sum);
});

router.post('/while', (req, res) => {
    const sum = getWhileSum(req.body.list);
    res.json(sum);
});

router.post('/recursion', (req, res) => {
    const sum = getRecursionSum(req.body.list);
    res.json(sum);
});

module.exports = router;
