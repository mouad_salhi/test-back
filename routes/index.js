'use strict';

const express = require('express');
const router = express.Router();

router.get('/', function(req, res) {
    res.send("ok");
});

router.use("/default", route("default"));
router.use("/test1", route("calculator"));
router.use("/test2", route("zipper"));
router.use("/test3", route("matrix"));

module.exports = router;