'use strict';

const express = require('express');
const router = express.Router();

const getHighestPath = require('../domain/matrix/getHighestPath');

router.post('/', (req, res) => {
    const highestPath = getHighestPath(req.body.matrix);
    res.json(highestPath);
});


module.exports = router;
