'use strict';

const express = require('express');
const router = express.Router();

const zip = require('../domain/zipper/zip');

router.post('/', (req, res) => {
    const { list1, list2 } = req.body;
    const zippedList = zip(list1, list2);

    res.json(zippedList);
});

module.exports = router;
